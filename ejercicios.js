/*//E1
let frase = "Muad'Dib who ordered battle drums from his enemies' skins".split(' ');
console.log(frase.length);
console.log(frase[Math.floor(frase.length/2)]);
console.log(frase[Math.ceil(frase.length/2)]);

//E2

let fraseWpp = 'He earns 5000 euro from salary per month, 10000 euro annual bonus, 15000 euro online courses per month'.split(' ');
console.log(fraseWpp);
console.log(+fraseWpp[2],+fraseWpp[8],+fraseWpp[12]);

//E3
const sentence = '%I $am@% a %tea@cher%, &and& I lo%#ve %te@a@ching%;. The@re $is no@th@ing; &as& mo@re rewarding as educa@ting &and& @emp%o@weri@ng peo@ple. ;I found tea@ching m%o@re interesting tha@n any ot#her %jo@bs. %Do@es thi%s mo@tiv#ate yo@u to be a tea@cher!? %Th#is 30#Days&OfJavaScript &is al@so $the $resu@lt of &love& of tea&ching';
//let sentenceReplace = sentence.replaceAll('%','').replaceAll('$','').replaceAll('@','').replaceAll('&','').replaceAll('#','').replaceAll(';','');
let sentenceReplace = sentence.replaceAll(/\$|%|@|&|#|;/g,'').toUpperCase();
//console.log(sentenceReplace.toUpperCase());
console.log(sentenceReplace);


//E4
let loveSentence = 'Love is the best thing in this world. Some found their love and some are still looking for their love.';
let sentenceCount = (loveSentence.match(/Love|love/g)).length;
console.log(sentenceCount);

//E5
let compare1 = 4 > 3; //true
let compare2 = 4 >= 3; //true
let compare3 = 4 < 3; //false
let compare4 = 4 <= 3; //false
let compare5 = 4 == 4; //true
let compare6 = 4 === 4; //true
let compare7 = 4 != 4; //false
let compare8 = 4 !== 4; //false
let compare9 = 4 != '4'; //false
let compare10 = 4 == '4'; //true
let compare11 = 4 === '4'; //false
let compare12 = 4 > 3 && 10 < 12; //true
let compare13 = 4 > 3 && 10 > 12; //false
let compare14 = 4 > 3 || 10 < 12; //true
let compare15 = 4 > 3 || 10 > 12; //true
let compare16 = !(4 > 3); //false
let compare17 = !(4 < 3); //true
let compare18 = !(false); //true
let compare19 = !(4 > 3 && 10 < 12); //false
let compare20 = !(4 > 3 && 10 > 12); //true
let compare21 = !(4 === '4'); //true

console.log(`1: ${compare1}\n2: ${compare2}\n3: ${compare3}\n4: ${compare4}\n5: ${compare5}\n6: ${compare6}\n7: ${compare7}\n8: ${compare8}\n9: ${compare9}\n10: ${compare10}\n11: ${compare11}\n12: ${compare12}\n13: ${compare13}\n14: ${compare14}\n15: ${compare15}\n16: ${compare16}\n17: ${compare17}\n18: ${compare18}\n19: ${compare19}\n20: ${compare20}\n21: ${compare21}`);

//E6

let year = '0000';
let month = 'FebruarY';

if (year.match(/^[\d]{4}$/)){
    if (month.match(/^[aA-zZ]/g)){
        const months = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"]
        let monthIndex = months.indexOf(month.toLowerCase())+1
        if (monthIndex != 0 ){ 
            const dayCount = new Date(year, monthIndex, 0).getDate()
            console.log(dayCount)
        } else {
            console.log('Please enter a valid month')
        }
    } else {
        console.log('Please enter a valid month')
    }
} else {
    console.log('Please enter a valid year')
}

//refactor
let year = '2001'
let month = 'FebruarY'
const months = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"]

if (!year.match(/^[\d]{4}$/)){
    console.log('Please enter a valid year')
    return
};

let monthIndex = months.indexOf(month.toLowerCase())+1

if (monthIndex === 0 ) {
    console.log('Please enter a valid month')
    return
}   

const dayCount = new Date(year, monthIndex, 0).getDate()
console.log(dayCount)

//E7 Prime Numbers

let num1 = 0
let num2 = 100
let primeArray = []

outer: for (let prime = num1; prime <= num2; prime++){
    for (let check = 2; check < prime; check++){
        if (prime % check === 0) continue outer
    }
    primeArray.push(prime)
}

console.log(`The prime numbers between ${num1} and ${num2} are ${primeArray}`)

//E8 Number randomizer

let numbersArray = []
let arrayLength = 5

do{
    let random = Math.floor(Math.random()*200)
    if (!numbersArray.includes(random)){
        numbersArray.push(random)
    }
}while(numbersArray.length < arrayLength)

console.log(numbersArray)

//E9 Christmas Tree

let christmasTree = '*'
let layers = 10
let padding = layers+1

for (i = 0; i <=layers; i++){
    let christmasTreePad = christmasTree.padStart(padding++, ' ')
    console.log(christmasTreePad)
    christmasTree += '**'
} 

//E10 Shopping Cart

const shoppingCart = ["Milk", "Coffee", "Tea", "Honey"]
shoppingCart.unshift("Meat")
shoppingCart.push("Sugar")
shoppingCart[3] = "Green Tea"
console.log(shoppingCart)

//E11 String to Array

const text = "I love teaching and empowering people. I teach HTML, CSS, JS, React, Phyton"
let textReplace = text.replaceAll(/\W\s/g, ' ')
let words = textReplace.split(' ')
console.log(words)
console.log(words.length)

//E12 ages

const ages = [19, 22, 19, 24, 20, 25, 26, 24, 25, 24]
ages.sort(function(a,b){return a - b})
minAge = ages[0]
maxAge = ages[ages.length-1]
ageMedian = ages[Math.floor(ages.length/2)]/2
ageAverage = ages.reduce((accumulator, currentValue)=>{return accumulator + currentValue})/ages.length
ageRange = ages[ages.length-1]-ages[0]
minAgeAbs = Math.abs(minAge - ageAverage)
maxAgeAbs = Math.abs(maxAge - ageAverage)
console.log(`The min age in ages is ${minAge} and the max age is ${maxAge}`)
console.log(`The median age of ages is ${ageMedian}`)
console.log(`The average age of ages is ${ageAverage}`)
console.log(`The range of ages in ages is ${ageRange}`)
console.log(`Is the absolute value of min - avergae the same as max - avergae? ${minAgeAbs === maxAgeAbs}`)

//E13 Number Ratio

let ratioArray = []

function randomNumberArray(arrayMin, arrayMax){
   
    let arrayLength = arrayMin
    
    do{
        let random = Math.floor(Math.random()*arrayMax)-arrayLength
        if (!ratioArray.includes(random)){
            ratioArray.push(random)
        }
    }while(ratioArray.length < arrayLength)
    
    console.log(`For the given array ${ratioArray} the ratios are the following:`)
}

randomNumberArray(5,10)

function arrayRatio () {

    let positives = 0
    let negatives = 0
    let zeroes = 0

    for (i in ratioArray){
 
        if(ratioArray[i] > 0 ){
            positives += 1
            } else if (ratioArray[i] < 0){
                negatives += 1
            } else {
                 zeroes +=1
            }               
    } 
    console.log(`The positive numbers count is ${positives} and the proportion of occurence is ${(positives/ratioArray.length).toPrecision(6)}`)
    console.log(`The negative numbers count is ${negatives} and the proportion of occurence is ${(negatives/ratioArray.length).toPrecision(6)}`)
    console.log(`The zeroes count is ${zeroes} and the proportion of occurence is ${(zeroes/ratioArray.length).toPrecision(6)}`)
}

arrayRatio()

//E14 Min and Max value

let positiveIntergers = []

function randomPositivesArray(){
   
    let arrayLength = 5
    
    do{
        let random = Math.floor(Math.random()*20)
        if (!positiveIntergers.includes(random)){
            positiveIntergers.push(random)
        }
    }while(positiveIntergers.length < arrayLength)
    
    console.log(`For the given array ${positiveIntergers}`)
}

randomPositivesArray()

function minMaxSum (){
    let sumArray = []

    for (i in positiveIntergers){
        let sum = 0
        sum = positiveIntergers.reduce((initialValue, accumulator) => initialValue + accumulator) - positiveIntergers[i]
        sumArray.push(sum)
    }

    console.log(`- The minimum sum is ${Math.min.apply(null, sumArray)}`)
    console.log(`- The maximum sum is ${Math.max.apply(null, sumArray)}`)

}

minMaxSum()*/

//E15 Hour Conversion

let time = new Date()

function timeConvertion(){
    let amPmTime = ""
    let militaryTime = ""
    amPmTime = time.toLocaleString('en-US',{hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true})
    militaryTime = time.toLocaleString('en-US',{hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false})
    console.log(`The time now in a 12H format is ${amPmTime} and the same time in military format is ${militaryTime}`)
}

timeConvertion()

/*let amPmTime = "11:25:40 AM"

function timeConvertion(){
    let militaryTime = ""
    if (/^(.*1)(.*2){2}/.test(amPmTime) && amPmTime.endsWith("AM")){
        militaryTime = amPmTime.replace(/^[12]{2}/, "00")
        militaryTime = militaryTime.replace(/.{2}$/,"").trimEnd()
    } else if(!/^(.*1)(.*2){2}/.test(amPmTime) && amPmTime.endsWith("PM")){
        let hour = +amPmTime.slice(0,2) + 12
        militaryTime = amPmTime.replace(/^.{2}/, hour)
        militaryTime = militaryTime.replace(/.{2}$/,"").trimEnd()
    }else { 
        militaryTime = amPmTime.replace(/.{2}$/,"").trimEnd()
    }
    console.log(`The time now in a 12H format is ${amPmTime} and the same time in military format is ${militaryTime}`)
}

timeConvertion()*/

